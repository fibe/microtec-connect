import tensorflow
import tensorflow as tf
import h5py
from tensorflow.keras.models import model_from_json
from tensorflow.keras.utils import plot_model
import matplotlib.pyplot as plt
import pickle
import numpy as np
from tqdm import trange


def evaluate_dataset( datasetname, model, WIN_SIZE, drawplots = True, saveplots=False, verbose=True, model_name=None ):
    
    x_test = None
    y_test = None
    y_pred = None
    
    with h5py.File(datasetname,'r') as f:
        x_test = np.array( f["x"] )
        y_test = np.array( f["y"] )
        y_pred = model.predict( x_test )
        
        np.savetxt("y_pred.txt",y_pred)
        np.savetxt("y_test.txt",y_test)
                
        return evaluate_dataset_helper( y_pred, y_test, WIN_SIZE, drawplots=drawplots, saveplots=saveplots, verbose=verbose, model_name=model_name )
    

    
def evaluate_dataset_helper( y_pred, y_gt, WIN_SIZE, drawplots = True, saveplots=False, verbose=True, dc_threshold=0.8, model_name=None ):

    # Classifier eval
    gt_yes = (y_gt[:,0]>dc_threshold).astype(np.uint8)
    pred_yes = (y_pred[:,0]>dc_threshold).astype(np.uint8)
    gt_no = 1-gt_yes
    pred_no = 1-pred_yes

    tp = np.sum( gt_yes*pred_yes )
    tn = np.sum( gt_no*pred_no )
    fp = np.sum( gt_no*pred_yes)
    fn = np.sum( gt_yes*pred_no)

    Nsamples = y_gt.shape[0]
    assert tp+tn+fp+fn==Nsamples

    if verbose:
        print("Confusion matrix (%):")
        print("=-=-=-=-=-=-=-=-=-=-=-")
        print("                         Yes       No   <-    Predicted peak ")
        print("Actual peak  Yes  >    %1.4f     %1.4f"%(tp/Nsamples*100,fn/Nsamples*100))
        print("              No  >    %1.4f     %1.4f"%(fp/Nsamples*100,tn/Nsamples*100))
        print("")
        
    accuracy = (tp+tn)/Nsamples
    precision = 0
    recall = 0

    if tp+fp > 0:
        precision = tp / (tp+fp)

    if tp+fn > 0:
        recall = tp / (tp+fn)

    if verbose:
        print(" Accuracy: %1.5f"%accuracy )
        print("Precision: %1.5f"%precision )
        print("   Recall: %1.5f"%recall )

    position_errors = []
    position_v_errors = []
    ecc_errors = []
    peak_errors = []
    angle_errors = []

    correct_samples = np.argwhere( gt_yes*pred_yes )
    kk=0
    for ii in correct_samples:
        sample_gt = np.squeeze( y_gt[ii,...] )
        sample_pred = np.squeeze( y_pred[ii,...] )

        mu_delta_pred = sample_pred[1:3]
        mu_pred = np.array([ WIN_SIZE//2,WIN_SIZE//2] ) + mu_delta_pred*(WIN_SIZE/2)
        ecc_pred = sample_pred[3]
        peak_pred = sample_pred[4]                
        angle_pred = np.arctan2(sample_pred[6],sample_pred[5])*0.5
        if angle_pred<0:
            angle_pred += np.pi*0.5

        mu_delta_gt = sample_gt[1:3]
        mu_gt = np.array([ WIN_SIZE//2,WIN_SIZE//2] ) + mu_delta_gt*(WIN_SIZE/2)
        ecc_gt = sample_gt[3]
        peak_gt = sample_gt[4]                
        angle_gt = np.arctan2(sample_gt[6],sample_gt[5])*0.5
        if angle_gt<0:
            angle_gt += np.pi*0.5


        position_error = np.linalg.norm( mu_pred-mu_gt )
        ecc_error = ecc_pred-ecc_gt
        peak_error = peak_pred-peak_gt
        angle_error = min( angle_pred-angle_gt, np.pi-(angle_pred-angle_gt) )

        position_errors.append( position_error )
        position_v_errors.append( mu_pred-mu_gt )
        ecc_errors.append( ecc_error )
        peak_errors.append( peak_error )
        angle_errors.append( angle_error )

        """
        I = np.squeeze( f["x"][ii,...] )
        plt.figure( figsize=(10,10) )
        plt.imshow(I, vmin=0, vmax=255, cmap="gray")
        plt.scatter( mu_pred[0], mu_pred[1] )
        plt.scatter( mu_gt[0], mu_gt[1] )
        plt.legend(["pred","gt"])
        plt.title("Ecc: %1.2f/%1.2f, peak: %d/%d, angle: %3.1f/%3.1f, err: %2.1f"%(ecc_pred,ecc_gt,peak_pred,peak_gt,angle_pred*180.0/np.pi, angle_gt*180.0/np.pi, angle_error*180/np.pi) )

        kk+=1
        if kk>10:
            break
        """


    position_errors = np.array(position_errors)
    position_v_errors = np.array( position_v_errors )
    ecc_errors = np.array(ecc_errors)
    peak_errors = np.array( peak_errors )
    angle_errors = np.array(angle_errors)*180/np.pi # to degrees

    if drawplots:
        plt.figure( figsize=(11,14))
        plt.subplot(3,2,1)
        plt.scatter( position_v_errors[:,0], position_v_errors[:,1] )
        plt.grid("minor")
        plt.axis("equal")
        plt.title("Position errors (px)")
        
        plt.subplot(3,2,2)
        plt.hist( position_errors )
        plt.title("Peak position error mean=%f, std=%f"% (np.mean(position_errors), np.std(position_errors)))
        plt.xlabel("Error (px)")
        plt.ylabel("Num samples")
        plt.grid()
        
        plt.subplot(3,2,3)
        plt.hist( ecc_errors )
        plt.title("Eccentricity error mean=%f, std=%f"% (np.mean(ecc_errors), np.std(ecc_errors)))
        plt.xlabel("Error")
        plt.ylabel("Num samples")
        plt.grid()
        
        plt.subplot(3,2,4)
        plt.hist( peak_errors )
        plt.title("Peak height error mean=%f, std=%f"% (np.mean(peak_errors), np.std(peak_errors)))
        plt.xlabel("Error")
        plt.ylabel("Num samples")
        plt.grid()
        

        plt.subplot(3,2,5)
        plt.hist( angle_errors )
        plt.title("Angle error mean=%f, std=%f"% (np.mean(angle_errors), np.std(angle_errors)))
        plt.xlabel("Error (deg)")
        plt.ylabel("Num samples")
        plt.grid()
        
        if not model_name is None:
            txt = "Model: "+model_name+"\n"
            txt += " Accuracy: %1.7f, Precision: %1.7f, Recall: %1.7f "%(accuracy,precision,recall) 
            plt.suptitle(txt, fontsize=16)

        if saveplots:                
            plt.savefig("model_evaluation.pdf", transparent=False, orientation="portrait")
        
    return (accuracy, 
            np.mean(position_errors), np.std(position_errors), 
            np.mean(ecc_errors), np.std(ecc_errors),
            np.mean(peak_errors), np.std(peak_errors),
            np.mean(angle_errors), np.std(angle_errors),
            precision, recall)