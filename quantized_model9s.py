import numpy as np
from scipy.signal import correlate
import h5py

from fractions import Fraction


class Quantizer:
    
    def __init__( self, scale=1.0, zero=0.0, nbits=8 ):
        self.S = scale
        self.Z = zero
        self.nbits = nbits
        self.minqval = -2**(nbits-1)+1
        self.maxqval =  2**(nbits-1)-1
        self.minrval = (self.minqval-self.Z)*self.S
        self.maxrval = (self.maxqval-self.Z)*self.S
        
        
    def _s(a,b,nbits):
        return (b-a)/(2**nbits-1)

    
    def to_real( self, q ):
        return self.S*(q-self.Z).astype(np.float32)

    
    def to_quant( self, r):
        q = np.clip( r/self.S + self.Z, self.minqval, self.maxqval )
        return q.astype(np.int32)
    
    
    def get_quantizer_from_range( minv, maxv, nbits, keepzero=True ):
        if keepzero:
            b = np.amax( (np.abs(minv),np.abs(maxv)) )
            S = b/(2**nbits-1)*2
            Z = 0
        else: 
            a = minv
            b = maxv
            S = Quantizer._s(a,b,nbits)
            Z = int( np.round( -a/S - (2**nbits-1)/2 ) )
            
        return Quantizer( S, Z, nbits )
    
    
    def get_optimal_quantizer( tensor, nbits, keepzero=True ):
        return Quantizer.get_quantizer_from_range( np.amin(tensor), np.amax(tensor), nbits, keepzero )
    
    
    def requantize( self, q, newquantizer ):
        sfac = self.S / newquantizer.S        
        sfac_frac = Fraction(sfac).limit_denominator( 2**16 )
        off = int(-self.Z*self.S/newquantizer.S + newquantizer.Z)        
        qp = q * sfac_frac.numerator // sfac_frac.denominator + off
        np.clip( qp, newquantizer.minqval, newquantizer.maxqval ).astype(np.int32)
        return qp
    
    
    
    
class QuantizedConv2d:
    def __init__(self, kernel, nbits ):
        
        self.kernel = kernel
        
        self.kernel_quantizer = []
        self.kernel_q = []
        self.zvals_sum = np.zeros( (self.kernel.shape[-1], 1), dtype=np.float32  )
        
        for filter_idx in range(self.kernel.shape[-1]):
            
            K = self.kernel[:,:,filter_idx]
            q = Quantizer.get_optimal_quantizer( K, nbits, keepzero=True )
            
            self.kernel_quantizer.append( q )
            self.kernel_q.append( q.to_quant( K ) )
            
            zvals = np.zeros( self.kernel.shape[1] )
            
            for channel_idx in range(self.kernel.shape[1]):
                zvals[channel_idx] = np.sum(self.kernel[:, channel_idx, filter_idx])
            
            self.zvals_sum[filter_idx] = np.sum( zvals )
            
        
    def __call__( self, input_data, input_quantizer ):
        
        output_quantizer = None
        outsize = input_data.shape[0]
        
        if input_data.dtype==np.int32:
            #print("Quantized mode")
            out = np.zeros( (self.kernel.shape[-1], outsize), dtype=np.int32 )
            
            minrvals = np.zeros( (self.kernel.shape[-1], 1), dtype=np.float32 )
            maxrvals = np.zeros( (self.kernel.shape[-1], 1), dtype=np.float32 )
            out_quantizers = []
            
            out_q = np.zeros( (self.kernel.shape[-1], outsize), dtype=np.int32 )
                        
            for filter_idx in range(self.kernel.shape[-1]):
                zvals = np.zeros( self.kernel.shape[1] )
                
                for channel_idx in range(self.kernel.shape[1]):
                    corr_in = input_data[:, channel_idx]
                    kern_in = (self.kernel_q[filter_idx])[:,channel_idx]
                    
                    
                    # Note convolution outputs before re-quantization must keep more bits than input/kernel bits (in this case int32)
                    out[filter_idx, :] += correlate( np.pad(corr_in, kern_in.shape[0]//2, mode='constant', constant_values=input_quantizer.Z ), kern_in, mode="valid" ) 

                    
                filter_quantizer = Quantizer( input_quantizer.S * self.kernel_quantizer[filter_idx].S, 
                                              self.zvals_sum[filter_idx]*input_quantizer.Z/self.kernel_quantizer[filter_idx].S, 
                                              input_quantizer.nbits )
                
                minrvals[filter_idx] = (np.amin( out[filter_idx, :] )-filter_quantizer.Z) * filter_quantizer.S 
                maxrvals[filter_idx] = (np.amax( out[filter_idx, :] )-filter_quantizer.Z) * filter_quantizer.S 
                out_quantizers.append( filter_quantizer )            
            
            # Get "optimal" output quantizer
            out_global_min = np.amin( minrvals )
            out_global_max = np.amax( maxrvals )
            if out_global_max == out_global_min:
                out_global_max = out_global_min+1E-3
                
            out_quantizer = Quantizer.get_quantizer_from_range( out_global_min, out_global_max, input_quantizer.nbits, keepzero=False  )
            #print("Global min/max", out_global_min, "/", out_global_max)
            
            # Requantize all outputs
            filter_idx = 0
            for q in out_quantizers:
                out_q[filter_idx, :] = q.requantize( out[filter_idx, :], out_quantizer )
                filter_idx += 1
            return out_q.T, out_quantizer
            
        else: 
            #print("float32 mode")
            
            out = np.zeros( (self.kernel.shape[-1], outsize), dtype=np.float32 )

            for filter_idx in range(self.kernel.shape[-1]):
                for channel_idx in range(self.kernel.shape[1]): 
                    corr_in = input_data[:, channel_idx]
                    kern_in = self.kernel[:, channel_idx, filter_idx]
                    out[filter_idx, :] += correlate( np.pad(corr_in,int(kern_in.shape[0]/2),mode='constant',constant_values=0), kern_in, mode="valid" )

        return out.T, None
    
    


class QuantizedBias:
    def __init__(self, bias ):        
        self.bias = bias
        self.bias_quantizer = Quantizer.get_optimal_quantizer( self.bias, 8, keepzero=True )
        self.bias_q1 = self.bias_quantizer.to_quant(self.bias)
        
        
    def __call__( self, input_data, input_quantizer ):
        if input_data.dtype==np.int32:
            #print("Quantized mode")
                        
            # Requantize Bias to match input_quantizer
            bias_q = self.bias_quantizer.requantize( self.bias_q1, input_quantizer )
            
            # Apply Bias
            out = input_data + bias_q
            assert out.dtype == np.int32
            
            # Compute the new optimal quantizer (ie. output rescale)
            maxabsval = np.ceil( np.amax( np.abs(out) ))
            if maxabsval==0:
                newscale_real = 1.0
            else:
                newscale_real = (2**(input_quantizer.nbits-1)-1)/maxabsval
            
            newscale = Fraction( newscale_real  ).limit_denominator()
            
            out =  np.clip(out * newscale.numerator // newscale.denominator, input_quantizer.minqval, input_quantizer.maxqval ).astype(np.int32)
            
            output_quantizer = Quantizer( input_quantizer.S/newscale_real,
                                          2*input_quantizer.Z*newscale_real,
                                          input_quantizer.nbits)
            
            return out, output_quantizer
        else:
            #print("float32 mode")
            return input_data + self.bias, None
        
        
class QuantizedRelu:
    def __init__( self  ):
        pass
        
    def __call__( self, input_data, input_quantizer ):
        if input_data.dtype==np.int32:
            #print("Quantized mode")
            thresh = int(input_quantizer.Z)
            out = np.maximum(input_data, thresh, input_data)          
            return out, input_quantizer
        
        else:
            #print("float32 mode")
            return np.maximum(input_data, 0, input_data), None

        
        
def sigmoid(x):
    return 1 / (1 + np.exp(-x))



def max_pooling(mat, ksize):
    m, n = mat.shape[:2]

    ny = m // ksize
    mat_pad = mat[:ny * ksize, ...]

    new_shape = (ny, ksize, n) + mat.shape[2:]
    return np.nanmax(mat_pad.reshape(new_shape), axis=1)



class CNN:
    def __init__(self, cnnweigths, nbits=8 ):
        
        self.layers = []
        self.nbits = nbits
        
        with h5py.File(cnnweigths,"r") as f:
            
            self.C0K = np.array(f["/C0/C0/kernel:0"])
            self.C0B = np.array(f["/C0/C0/bias:0"])
            
            self.C1K = np.array(f["/C1/C1/kernel:0"])
            self.C1B = np.array(f["/C1/C1/bias:0"])
            
            self.C2K = np.array(f["/C2/C2/kernel:0"])
            self.C2B = np.array(f["/C2/C2/bias:0"])
            
            self.C3K = np.array(f["/C3/C3/kernel:0"])
            self.C3B = np.array(f["/C3/C3/bias:0"])
            
            self.RC4K = np.array(f["/RC4/RC4/kernel:0"])
            self.RC4B = np.array(f["/RC4/RC4/bias:0"])
            
            self.RC5K = np.array(f["/RC5/RC5/kernel:0"])
            self.RC5B = np.array(f["/RC5/RC5/bias:0"])
            
            self.RC6K = np.array(f["/RC6/RC6/kernel:0"])
            self.RC6B = np.array(f["/RC6/RC6/bias:0"])
            
            self.conv_featprobK = np.array(f["/featprob/featprob/kernel:0"])
            self.conv_featprobB = np.array(f["/featprob/featprob/bias:0"])
            
            self.conv_dispK = np.array(f["/disp/disp/kernel:0"])
            self.conv_dispB = np.array(f["/disp/disp/bias:0"])
            
            self.conv_descK = np.array(f["/desc/desc/kernel:0"])
            self.conv_descB = np.array(f["/desc/desc/bias:0"])
            
                        
            self.input_quantizer = Quantizer()
            
            self.layer_C0 = QuantizedConv2d( self.C0K, self.nbits )
            self.layer_C0B = QuantizedBias( self.C0B )
            self.layer_C0Relu = QuantizedRelu(  )
            
            self.layer_C1 = QuantizedConv2d( self.C1K, self.nbits )
            self.layer_C1B = QuantizedBias( self.C1B )
            self.layer_C1Relu = QuantizedRelu( )
            
            self.layer_C2 = QuantizedConv2d( self.C2K, self.nbits )
            self.layer_C2B = QuantizedBias( self.C2B )
            self.layer_C2Relu = QuantizedRelu( )
            
            self.layer_C3 = QuantizedConv2d( self.C3K, self.nbits )
            self.layer_C3B = QuantizedBias( self.C3B )
            self.layer_C3Relu = QuantizedRelu( )
            
            self.layer_RC4 = QuantizedConv2d( self.RC4K, self.nbits )
            self.layer_RC4B = QuantizedBias( self.RC4B )
            self.layer_RC4Relu = QuantizedRelu(  )
            
            self.layer_RC5 = QuantizedConv2d( self.RC5K, self.nbits )
            self.layer_RC5B = QuantizedBias( self.RC5B )
            self.layer_RC5Relu = QuantizedRelu(  )
            
            self.layer_RC6 = QuantizedConv2d( self.RC6K, self.nbits )
            self.layer_RC6B = QuantizedBias( self.RC6B )
            self.layer_RC6Relu = QuantizedRelu(  )            
            
            self.layer_conv_featprobK = QuantizedConv2d( self.conv_featprobK, self.nbits )
            self.layer_conv_featprobB = QuantizedBias( self.conv_featprobB )
            
            self.layer_conv_dispK = QuantizedConv2d( self.conv_dispK, self.nbits )
            self.layer_conv_dispB = QuantizedBias( self.conv_dispB )
            
            self.layer_conv_descK = QuantizedConv2d( self.conv_descK, self.nbits )
            self.layer_conv_descB = QuantizedBias( self.conv_descB )
            self.layer_descRelu = QuantizedRelu(  )   
            
            
            

    def __call__(self, x, quantize=True ):
        
        input_quantizer = None
        if quantize:
            input_quantizer = Quantizer.get_optimal_quantizer( x, self.nbits, keepzero=False )
            x = input_quantizer.to_quant(x)
            
        _l, q = self.layer_C0(x, input_quantizer )
        _l, q = self.layer_C0B(_l, q)
        _l, q = self.layer_C0Relu(_l, q)        
        
        _l, q = self.layer_C1(_l, q)
        _l, q = self.layer_C1B(_l, q)
        _l, q = self.layer_C1Relu(_l, q)
        _l = max_pooling(_l, 2)
                
        _l, q = self.layer_C2(_l, q)
        _l, q = self.layer_C2B(_l, q)
        _l, q = self.layer_C2Relu(_l, q)
        _l = max_pooling(_l, 2)
        
        _l, q = self.layer_C3(_l, q)
        _l, q = self.layer_C3B(_l, q)
        _l, _topq = self.layer_C3Relu(_l, q)
        _topl = max_pooling(_l, 2)
        
        
        _l, q = self.layer_RC4(_topl,_topq)
        _l, q = self.layer_RC4B(_l, q)
        _l, q = self.layer_RC4Relu(_l, q)
        _l = max_pooling(_l, 2)
        _l, q = self.layer_conv_featprobK(_l, q)
        _l, q = self.layer_conv_featprobB(_l, q)
        if not q is None:
            _l = q.to_real(_l)
        _prob = sigmoid( _l )        
        
        
        _l, q = self.layer_RC5(_topl,_topq)
        _l, q = self.layer_RC5B(_l, q)
        _l, q = self.layer_RC4Relu(_l, q)
        _l = max_pooling(_l, 2)
        _l, q = self.layer_conv_dispK(_l, q)
        _l, q = self.layer_conv_dispB(_l, q)
        if not q is None:
            _l = q.to_real(_l)
        _disp = np.tanh( _l )
        
        
        _l, q = self.layer_RC6(_topl,_topq)
        _l, q = self.layer_RC6B(_l, q)
        _l, q = self.layer_RC6Relu(_l, q)
        _l = max_pooling(_l, 2)
        _l, q = self.layer_conv_descK(_l, q)
        _l, q = self.layer_conv_descB(_l, q)
        _desc, q = self.layer_descRelu(_l, q)
        if not q is None:
            _desc = q.to_real(_desc)
        
        _out = np.concatenate([_prob, _disp, _desc], axis=1)
        
        return _out