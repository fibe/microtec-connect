import tensorflow
import tensorflow as tf
import h5py
from tensorflow.keras.models import model_from_json
from tensorflow.keras.utils import plot_model
import matplotlib.pyplot as plt
import scipy.signal
import pickle
from DataTensorConverter import DataTensorConverter
import numpy as np
from tqdm import trange



def evaluate_dataset( datasetname, model, drawplots = True, saveplots=False, verbose=True, model_name=None ):
    
    x_test = None
    y_test = None
    y_pred = None
    
    with h5py.File(datasetname,'r') as f:
        x_test = np.array( f["x"] )
        y_test = np.array( f["y"] )
        y_pred = model.predict( x_test )
        input_data = np.array( x_test )
        
        return evaluate_dataset_helper( y_pred, y_test, drawplots=drawplots, saveplots=saveplots, verbose=verbose, model_name=model_name )



def evaluate_dataset_helper( y_pred, y_test, drawplots = True, saveplots = False, verbose=True, dc_threshold=0.8, model_name=None ):
    
    with open("data/datatensorconverter.pickle","rb") as f:
        dc = pickle.load(f)
        dc.threshold=dc_threshold
                
    true_positive=0
    true_negative=0
    false_positive=0
    false_negative=0

    total_samples=0

    #print("Dim Y")
    #print(y_pred.shape)

    pred_shifts = []
    shift_error = []

    pos_errors = []
    height_errors = []
    std_errors = []

    alldatagt = np.zeros((y_pred.shape[0],11), dtype=np.float32 )
    alldatapred = np.zeros_like(alldatagt)
    alldatagt[:,0]=1024
    alldatapred[:,0]=1024


    for N in range( y_pred.shape[0] ):

        #print("Predicted Raw data:")
        #print(y_pred[N,...])
        #print("Gt Raw data:")
        #print(y_test[N,...])

        pred_data = dc.to_data( y_pred[N,...] )  

        #print("Predicted data: ")
        #print( pred_data )
        gtdata = dc.to_data(  y_test[N,...]  )
        #print("GT data: ")
        #print( gtdata )

        alldatagt[N,1:] = gtdata
        alldatapred[N,1:] = pred_data

        #fgt.write("%.3f "%gtdata)
        #fpred.write("%.3f "%pred_data)
        
        predicted_yes = (y_pred[N,:,0] >= dc.threshold).astype( np.uint8 )
        
        # Take at most 3 output samples exceeding the threshold (we assume to have at most 3 peaks)
        sorting_idx = np.argsort( y_pred[N,:,0] )
        predicted_yes[ sorting_idx[:-3] ] = 0
        
        predicted_no = 1-predicted_yes
        gt_yes = (y_test[N,:,0] >= dc.threshold).astype( np.uint8 )
        gt_no = (y_test[N,:,0] < dc.threshold).astype( np.uint8 )
        
        assert np.sum( gt_yes ) <= 3, "Max 3 peaks allowed in the test set"
        
        true_positive_indices = np.argwhere( predicted_yes*gt_yes )
        
        true_positive += np.sum(predicted_yes*gt_yes)
        true_negative += np.sum(predicted_no*gt_no)
        false_positive += np.sum(predicted_yes*gt_no)
        false_negative += np.sum(predicted_no*gt_yes)
        total_samples += predicted_yes.shape[0]
        
        for jj in true_positive_indices:
            
            pred_disp, pred_height, pred_std, pred_peakpos = dc.vals_from_tensor( jj, y_pred[N,...] )
            gt_disp, gt_height, gt_std, gt_peakpos = dc.vals_from_tensor( jj, y_test[N,...] )
            
            pred_shifts.append(pred_disp);
            shift_error.append(pred_disp - gt_disp);
            pos_errors.append( pred_peakpos-gt_peakpos  )
            
            height_errors.append( pred_height-gt_height  )
            std_errors.append( pred_std-gt_std  )
        
        """
        if pred_data[0]==gtdata[0]:
            true_positive += pred_data[0]
            true_negative += (3-pred_data[0])

            indices = np.argwhere( y_test[N,:,0] >= dc.threshold )
            for jj in indices:
                pred_shifts.append(y_pred[N,jj,1]);
                shift_error.append(y_pred[N,jj,1] - y_test[N,jj,1]);

            for ii in range(int(pred_data[0])):
                peakpos=pred_data[ii*3+1]
                peakheight=pred_data[ii*3+2]
                peakstd=pred_data[ii*3+3]

                peakpos_gt=gtdata[ii*3+1]
                peakheight_gt=gtdata[ii*3+2]
                peakstd_gt=gtdata[ii*3+3]

                #if np.abs(peakpos-peakpos_gt)==0:
                #    print(pred_data)
                #    print(gtdata)

                #pos_errors.append( np.abs(peakpos-peakpos_gt)  )
                pos_errors.append( peakpos-peakpos_gt  )

                #height_errors.append( np.abs(peakheight-peakheight_gt)  )
                height_errors.append( peakheight-peakheight_gt  )

                #std_errors.append( np.abs(peakstd-peakstd_gt)  )
                std_errors.append( peakstd-peakstd_gt  )

        if pred_data[0]>gtdata[0]:
            diff = pred_data[0]-gtdata[0]
            false_positive += diff
            true_positive += gtdata[0]
            true_negative += (3-pred_data[0])

        if pred_data[0]<gtdata[0]:
            diff = gtdata[0]-pred_data[0]
            false_negative += diff
            true_positive += pred_data[0]
            true_negative += (3-gtdata[0])

        total_samples += 3
        """


    np.savetxt("cnn_eval_gt.txt", alldatagt)
    np.savetxt("cnn_eval_pred.txt", alldatapred)
    
    true_positive = float(true_positive)/float(total_samples)
    true_negative = float(true_negative)/float(total_samples)
    false_positive = float(false_positive)/float(total_samples)
    false_negative = float(false_negative)/float(total_samples)

    if verbose:
        print(true_positive+true_negative+false_positive+false_negative)
        print("Confusion matrix (%):")
        print("=-=-=-=-=-=-=-=-=-=-=-")
        print("                         Yes       No   <-    Predicted peak ")
        print("Actual peak  Yes  >    %1.4f     %1.4f"%(true_positive*100,false_negative*100))
        print("              No  >    %1.4f     %1.4f"%(false_positive*100,true_negative*100))
        print("")

    pos_errors = np.array( pos_errors )
    height_errors = np.array( height_errors )
    
    pos_mean = np.mean(pos_errors)
    pos_std = np.std(pos_errors)
    h_mean = np.mean(height_errors)
    h_std = np.std(height_errors)
    accuracy = (true_positive+true_negative)/(true_positive+true_negative+false_positive+false_negative)
    precision = 0
    recall = 0
    
    if true_positive+false_positive > 0:
        precision = true_positive / (true_positive+false_positive)
    
    if true_positive+false_negative > 0:
        recall = true_positive / (true_positive+false_negative)
        
    if verbose:
        print(" Accuracy: %1.5f"%accuracy )
        print("Precision: %1.5f"%precision )
        print("   Recall: %1.5f"%recall )
    hist_bins = np.linspace(-5,5,6);

    if drawplots:
        plt.figure( figsize=(15,10) )
        plt.subplot(2,2,1)
        plt.hist( np.array(pred_shifts), bins=100)
        plt.title("Predicted shifts")
        plt.xlabel("Shift (px)")
        plt.ylabel("Num samples")
        plt.grid()

        '''plt.figure( figsize=(10,5) )
        plt.hist( np.array(shift_error), bins=hist_bins)
        plt.title("Shifts Error")
        plt.xlabel("Shift error (px)")
        plt.ylabel("Num samples")
        plt.grid()'''

        plt.subplot(2,2,2)
        plt.hist( np.array(pos_errors), bins=np.linspace(-11,11,12)*0.2 )
        plt.title("Peak position error mean=%f, std=%f"% (pos_mean, pos_std))
        plt.xlabel("Error (px)")
        plt.ylabel("Num samples")
        plt.grid()
        #plt.xlim([-50, 50])

        plt.subplot(2,2,3)
        plt.hist( np.array(height_errors), bins=60 )
        plt.title("Peak height error mean=%f, std=%f"% (h_mean, h_std))
        plt.xlabel("Error")
        plt.ylabel("Num samples")
        plt.grid()
        #plt.xlim([-15, 15])

        plt.subplot(2,2,4)
        plt.hist( np.array(std_errors), bins=60 )
        plt.title("Peak std error distribution")
        plt.xlabel("Error")
        plt.ylabel("Num samples")
        plt.grid()
        #plt.xlim([-5, 5])
    
        if not model_name is None:
            txt = "Model: "+model_name+"\n"
            txt += " Accuracy: %1.3f, Precision: %1.3f, Recall: %1.3f "%(accuracy,precision,recall) 
            plt.suptitle(txt, fontsize=16)
        plt.savefig("model_evaluation.pdf", transparent=False, orientation="landscape" )
    
    return (accuracy, pos_mean, pos_std, h_mean, h_std, precision, recall)