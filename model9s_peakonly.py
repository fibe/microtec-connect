import pickle
import h5py
import numpy as np
from scipy.signal import correlate
from evaluator_pk import evaluate_dataset_helper
import tqdm


def conv2d(input_data, kernel, bias=0):
    out = np.zeros((kernel.shape[-1], input_data.shape[0]))
    for filter_idx in range(kernel.shape[-1]):
        for channel_idx in range(kernel.shape[1]):
            out[filter_idx, :] += correlate(input_data[:, channel_idx], kernel[:, channel_idx, filter_idx], mode='same')
    return out.T + bias


def relu(x):
    return np.maximum(x, 0, x)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def max_pooling(mat, ksize):
    m, n = mat.shape[:2]

    ny = m // ksize
    mat_pad = mat[:ny * ksize, ...]

    new_shape = (ny, ksize, n) + mat.shape[2:]
    return np.nanmax(mat_pad.reshape(new_shape), axis=1)


class CNN:
    def __init__(self, cnnweigths ):
                
        with h5py.File(cnnweigths,"r") as f:
            
            self.C0K = np.array(f["/C0/C0/kernel:0"], dtype=np.float16 )
            self.C0B = np.array(f["/C0/C0/bias:0"], dtype=np.float16 )
            
            self.C1K = np.array(f["/C1/C1/kernel:0"], dtype=np.float16)
            self.C1B = np.array(f["/C1/C1/bias:0"], dtype=np.float16)
            
            self.C2K = np.array(f["/C2/C2/kernel:0"], dtype=np.float16)
            self.C2B = np.array(f["/C2/C2/bias:0"], dtype=np.float16)
            
            self.C3K = np.array(f["/C3/C3/kernel:0"], dtype=np.float16)
            self.C3B = np.array(f["/C3/C3/bias:0"], dtype=np.float16)
            
            self.RC4K = np.array(f["/RC4/RC4/kernel:0"], dtype=np.float16)
            self.RC4B = np.array(f["/RC4/RC4/bias:0"], dtype=np.float16)
            
            self.conv_featprobK = np.array(f["/featprob/featprob/kernel:0"], dtype=np.float16)
            self.conv_featprobB = np.array(f["/featprob/featprob/bias:0"], dtype=np.float16)
         
            

    def __call__(self, x ):
        x = x.astype( np.float16 )
        x = conv2d( x, self.C0K, self.C0B )
        x = relu(x)
        x = conv2d( x, self.C1K, self.C1B )
        x = relu(x)
        x = max_pooling( x, 2 )
        x = conv2d( x, self.C2K, self.C2B )
        x = relu(x)
        x = max_pooling( x, 2 )
        x = conv2d( x, self.C3K, self.C3B )
        x = relu(x)
        x = max_pooling( x, 2 )
        x = conv2d( x, self.RC4K, self.RC4B )
        x = relu(x)
        x = conv2d( x, self.conv_featprobK, self.conv_featprobB )
        x = sigmoid(x)
        
        return x

    

    
def do_main():
    cnn = CNN( "data/last_model9pk_tf2_pruned.h5" )

    TESTSIZE=5000
    with h5py.File('data/test_pk_100k_128.h5', 'r') as f:
        x_test = np.array(f['x'])
        y_test = np.array( f["y"][:TESTSIZE,...] )
        y_pred = []
        for ii in tqdm.trange(TESTSIZE):

            x = x_test[ii,...]
            y_pred.append( cnn(x) )
            return 0

        y_pred = np.array( y_pred )
        print(y_pred.shape)

        (accuracy, precision, recall) = evaluate_dataset_helper( y_pred, y_test, dc_threshold=0.8 )

        
        
if __name__=="__main__":
    do_main()