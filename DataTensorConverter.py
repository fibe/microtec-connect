import numpy as np


class DataTensorConverter:
    
    def __init__( self, input_size=1024, out_size=128, threshold=1.0, max_height=None, max_std=None ):
        self.input_size = input_size
        self.out_size = out_size
        self.threshold = threshold
        self.max_height = max_height
        self.max_std = max_std

    def out_tensor_shape(self):
        return (self.out_size,4)
        
    def to_tensor( self, data ):
        """
        Output tensor has shape out_sizex4 where:

        0,:  is the binary vector denoting the presence/absence of a peak
        1,:  is the peak displacement [-0.5 ... 0.5]
        2,:  is the peak height       [ Positive ]
        3,:  is the peak std.         [ Positive ]

        """
        T = np.zeros( self.out_tensor_shape(), dtype=np.float32 ) 
        ratio = float(self.out_size)/float(self.input_size)

        for ii in range( int(data[0]) ):

            
            peakpos=data[ii*3+1]
            peakheight=data[ii*3+2]
            peakstd=data[ii*3+3]

            arraypos_f = float(peakpos)*ratio
            
            arraypos = int(np.floor( arraypos_f+0.5 ))
            displ = (arraypos_f-arraypos)*2 # displacements in [-1 .. 1]
            
            #print("")
            #print("      Peak: ", peakpos)
            #print("arraypos_f: ", arraypos_f)
            #print("  arraypos: ", arraypos)
            #print("     displ: ", displ)
            if T[arraypos,0] > 0:
                raise ValueError("Peaks too close.")
            
            T[arraypos,:] = np.array([1.0, 
                                      displ,
                                      peakheight if self.max_height is None else (peakheight/self.max_height),
                                      peakstd if self.max_std is None else (peakstd/self.max_std) ])
            
        return T


    def to_data( self, T ):

        indices = np.argwhere( T[:,0]>=self.threshold )
        data = np.zeros( (1+3*3), np.float32 )

        kk=0
        for ii in indices:
            if kk>=3:
                break

            data[0]+=1
            disp, height, std, peakpos = self.vals_from_tensor( ii, T )
            
            data[kk*3+1]=peakpos
            data[kk*3+2]=height if self.max_height is None else (height*self.max_height)
            data[kk*3+3]=std if self.max_std is None else (std*self.max_std)
            kk+=1
        return data
    
    def vals_from_tensor( self, ii, T ):
        ratio = float(self.input_size)/float(self.out_size)
        disp = T[ii,1]
        height = T[ii,2]
        std = T[ii,3]
        peakpos = (float(ii) + disp*0.5)*ratio
        return disp, height, std, peakpos
        
    
    
    def min_detectable_peak_distance(self):
        return float(self.input_size)/float(self.out_size)