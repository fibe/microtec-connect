import tensorflow
import tensorflow as tf
import h5py
from tensorflow.keras.models import model_from_json
from tensorflow.keras.utils import plot_model
import matplotlib.pyplot as plt
import scipy.signal
import pickle
from DataTensorConverter_pk import DataTensorConverter
import numpy as np
from tqdm import trange



def evaluate_dataset( datasetname, model, drawplots = True, saveplots=False, verbose=True, model_name=None ):
    
    x_test = None
    y_test = None
    y_pred = None
    
    with h5py.File(datasetname,'r') as f:
        x_test = np.array( f["x"] )
        y_test = np.array( f["y"] )
        y_pred = model.predict( x_test )
        input_data = np.array( x_test )
        
        return evaluate_dataset_helper( y_pred, y_test, drawplots=drawplots, saveplots=saveplots, verbose=verbose, model_name=model_name )



def evaluate_dataset_helper( y_pred, y_test, drawplots = True, saveplots = False, verbose=True, dc_threshold=0.8, model_name=None ):
    
    with open("data/datatensorconverter.pickle","rb") as f:
        dc = pickle.load(f)
        dc.threshold=dc_threshold
                
    true_positive=0
    true_negative=0
    false_positive=0
    false_negative=0

    total_samples=0


    pred_shifts = []
    shift_error = []

    pos_errors = []
    height_errors = []
    std_errors = []

    alldatagt = np.zeros((y_pred.shape[0],11), dtype=np.float32 )
    alldatapred = np.zeros_like(alldatagt)
    alldatagt[:,0]=1024
    alldatapred[:,0]=1024


    for N in range( y_pred.shape[0] ):

        #print("Predicted Raw data:")
        #print(y_pred[N,...])
        #print("Gt Raw data:")
        #print(y_test[N,...])

        pred_data = dc.to_data( y_pred[N,...] )  

        #print("Predicted data: ")
        #print( pred_data )
        gtdata = dc.to_data(  y_test[N,...]  )
        #print("GT data: ")
        #print( gtdata )

        alldatagt[N,1:] = gtdata
        alldatapred[N,1:] = pred_data

        #fgt.write("%.3f "%gtdata)
        #fpred.write("%.3f "%pred_data)
        
        predicted_yes = (y_pred[N,:,0] >= dc.threshold).astype( np.uint8 )
        
        # Take at most 3 output samples exceeding the threshold (we assume to have at most 3 peaks)
        sorting_idx = np.argsort( y_pred[N,:,0] )
        predicted_yes[ sorting_idx[:-3] ] = 0
        
        predicted_no = 1-predicted_yes
        gt_yes = (y_test[N,:,0] >= dc.threshold).astype( np.uint8 )
        gt_no = (y_test[N,:,0] < dc.threshold).astype( np.uint8 )
        
        assert np.sum( gt_yes ) <= 3, "Max 3 peaks allowed in the test set"
        
        true_positive_indices = np.argwhere( predicted_yes*gt_yes )
        
        true_positive += np.sum(predicted_yes*gt_yes)
        true_negative += np.sum(predicted_no*gt_no)
        false_positive += np.sum(predicted_yes*gt_no)
        false_negative += np.sum(predicted_no*gt_yes)
        total_samples += predicted_yes.shape[0]
        
        for jj in true_positive_indices:
            
            pred_peakpos = dc.vals_from_tensor( jj, y_pred[N,...] )
            gt_peakpos = dc.vals_from_tensor( jj, y_test[N,...] )
            
            pos_errors.append( pred_peakpos-gt_peakpos  )
            

    np.savetxt("cnn_eval_gt.txt", alldatagt)
    np.savetxt("cnn_eval_pred.txt", alldatapred)
    
    true_positive = float(true_positive)/float(total_samples)
    true_negative = float(true_negative)/float(total_samples)
    false_positive = float(false_positive)/float(total_samples)
    false_negative = float(false_negative)/float(total_samples)

    if verbose:
        print(true_positive+true_negative+false_positive+false_negative)
        print("Confusion matrix (%):")
        print("=-=-=-=-=-=-=-=-=-=-=-")
        print("                         Yes       No   <-    Predicted peak ")
        print("Actual peak  Yes  >    %1.4f     %1.4f"%(true_positive*100,false_negative*100))
        print("              No  >    %1.4f     %1.4f"%(false_positive*100,true_negative*100))
        print("")

    accuracy = (true_positive+true_negative)/(true_positive+true_negative+false_positive+false_negative)
    precision = 0
    recall = 0
    
    if true_positive+false_positive > 0:
        precision = true_positive / (true_positive+false_positive)
    
    if true_positive+false_negative > 0:
        recall = true_positive / (true_positive+false_negative)
        
    if verbose:
        print(" Accuracy: %1.5f"%accuracy )
        print("Precision: %1.5f"%precision )
        print("   Recall: %1.5f"%recall )
    
    return (accuracy, precision, recall)